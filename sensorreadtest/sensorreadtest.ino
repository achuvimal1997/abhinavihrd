int s, l, r, a;
void setup()
{
  Serial.begin(9600);
  pinMode(8, OUTPUT);
  pinMode(7, INPUT);  //8 and 7 for right sensor
  pinMode(9, OUTPUT);
  pinMode(10, INPUT); //10 and 9 for  central sensor
  pinMode(11, OUTPUT);
  pinMode(12, INPUT);//12 AND 11 for left sensor
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

}

void loop() {
  long d1, d2, d3, cm1, cm2, cm3;  //cm1 is right distance cm2 is central and cm3 is left
  digitalWrite(8, LOW);
  delayMicroseconds(2);
  digitalWrite(8, HIGH);
  delayMicroseconds(5);
  digitalWrite(8, LOW);
  d1 = pulseIn(7, HIGH);
  cm1 = microsecondsToCentimeters(d1);
  Serial.print(cm1);
  Serial.print("cm1");
  Serial.println();
  digitalWrite(9, LOW);
  delayMicroseconds(2);
  digitalWrite(9, HIGH);
  delayMicroseconds(5);
  digitalWrite(9, LOW);
  d2 = pulseIn(10, HIGH);
  cm2 = microsecondsToCentimeters(d2);
  Serial.print(cm2);
  Serial.print("cm2");
  Serial.println();
  digitalWrite(11, LOW);
  delayMicroseconds(2);
  digitalWrite(11, HIGH);
  delayMicroseconds(5);
  digitalWrite(11, LOW);
  d3 = pulseIn(12, HIGH);
  cm3 = microsecondsToCentimeters(d3);
  Serial.print(cm3);
  Serial.print("cm3");
  Serial.println();
  delay(100);
  int dif1; //difference between left and right
  if (cm1 > cm3)
  {
    dif1 = cm1 - cm3;
  }
  else
  {
    dif1 = cm3 - cm1;
  }
  if (cm2 < 5 ||  cm2 > 2) //measures hump and pothole change 5 to static value in serial monitor
  {
    s = 1;
    Serial.print("clear road");
  }
  else if (cm2 > 5) // adjust 5 and 2 using serial monitor
  {
    Serial.print("pothole detected");
    s = 0;
  }
  else if (cm2 < 2)
  {
    Serial.print("hump detected");
    s = 0;
  }
  if (dif1 < 10) //checks difference between left and right sensors are greater than 10 so that a bend is detected
  {
    l = 0;   // r and l are flags used to control turning
    r = 0;
    Serial.print("straight road");
  }
  else if (cm1 > cm3 && dif1 > 10)
  {
    r = 1;
    Serial.print("right turn");
  }
  else if (cm3 > cm1 && dif1 > 10)
  {
    l = 1;
    Serial.print("left turn");
  }
while (Serial.available() > 0)
  {
    a = Serial.read();
  }
  if (a == 1)
  {
    digitalWrite(2, HIGH);
    digitalWrite(3, LOW);
    digitalWrite(5, HIGH);
    digitalWrite(6, LOW);
    delay(1000);
  }
  else if (a == 2) //right
  {
    digitalWrite(2, HIGH);
    digitalWrite(3, LOW);
    digitalWrite(5, LOW);
    digitalWrite(6, HIGH);
    delay(1000);
  }
  else if (a == 3)//left
  {
    digitalWrite(2, LOW);
    digitalWrite(3, HIGH);
    digitalWrite(5, HIGH);
    digitalWrite(6, LOW);
    delay(1000);
  }
  else if (a == 4) //reverse
  {
    digitalWrite(3, HIGH);
    digitalWrite(2, LOW);
    digitalWrite(6, HIGH);
    digitalWrite(5, LOW);
    delay(1000);
  }
  else //stay stopped
  {
    digitalWrite(2, LOW);
    digitalWrite(3, LOW);
    digitalWrite(5, LOW);
    digitalWrite(6, LOW);
    delay(1000);
  }
}

long microsecondsToCentimeters(long microseconds) {
  return microseconds / 29 / 2;
}
