int s, l, r, a;
void setup()
{
  Serial.begin(9600);
  pinMode(8, OUTPUT);
  pinMode(7, INPUT);
  pinMode(9, OUTPUT);
  pinMode(10, INPUT);
  pinMode(11, OUTPUT);
  pinMode(12, INPUT);
  pinMode(21, OUTPUT);
  pinMode(22, OUTPUT);
  pinMode(23, OUTPUT);
  pinMode(24, OUTPUT);
  pinMode(25, OUTPUT);
  pinMode(26, OUTPUT);
}

void loop() {
    digitalWrite(21, HIGH);
    digitalWrite(22, HIGH);
    digitalWrite(23, LOW);
    digitalWrite(24, HIGH);
    digitalWrite(25, HIGH);
    digitalWrite(26, LOW);
    delay(1000);
    
    digitalWrite(21, HIGH);
    digitalWrite(23, HIGH);
    digitalWrite(22, LOW);
    digitalWrite(24, HIGH);
    digitalWrite(25, HIGH);
    digitalWrite(26, LOW);
    delay(1000);
    digitalWrite(21, HIGH);
    digitalWrite(22, HIGH);
    digitalWrite(23, LOW);
    digitalWrite(24, HIGH);
    digitalWrite(26, HIGH);
    digitalWrite(25, LOW);
    delay(1000);
    digitalWrite(21, HIGH);
    digitalWrite(23, HIGH);
    digitalWrite(22, LOW);
    digitalWrite(24, HIGH);
    digitalWrite(26, HIGH);
    digitalWrite(25, LOW);
    delay(1000);
    digitalWrite(21, LOW);
    digitalWrite(22, HIGH);
    digitalWrite(23, LOW);
    digitalWrite(24, LOW);
    digitalWrite(25, HIGH);
    digitalWrite(26, LOW);
    delay(1000);
}
