char a;
void setup()
{
  Serial.begin(9600);
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
}

void loop() {
  while (Serial.available() > 0)
  {
    a = Serial.read();
  }
  if (a == 1)
  {
    digitalWrite(2, HIGH);
    digitalWrite(3, LOW);
    digitalWrite(5, HIGH);
    digitalWrite(6, LOW);
    delay(1000);
  }
  else if (a == 2) //right
  {
    digitalWrite(2, HIGH);
    digitalWrite(3, LOW);
    digitalWrite(5, LOW);
    digitalWrite(6, HIGH);
    delay(1000);
  }
  else if (a == 3)//left
  {
    digitalWrite(2, LOW);
    digitalWrite(3, HIGH);
    digitalWrite(5, HIGH);
    digitalWrite(6, LOW);
    delay(1000);
  }
  else if (a == 4) //reverse
  {
    digitalWrite(3, HIGH);
    digitalWrite(2, LOW);
    digitalWrite(6, HIGH);
    digitalWrite(5, LOW);
    delay(1000);
  }
  else //stay stopped
  {
    digitalWrite(2, LOW);
    digitalWrite(3, LOW);
    digitalWrite(5, LOW);
    digitalWrite(6, LOW);
    delay(1000);
  }
}
