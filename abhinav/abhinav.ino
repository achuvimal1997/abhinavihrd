int s, l, r;
char a ;
void setup()
{
  Serial.begin(9600);
  pinMode(8, OUTPUT);
  pinMode(7, INPUT);
  pinMode(9, OUTPUT);
  pinMode(10, INPUT);
  pinMode(11, OUTPUT);
  pinMode(12, INPUT);

  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);

  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
}

void loop() {
  long d1, d2, d3, cm1, cm2, cm3;
  digitalWrite(8, LOW);
  delayMicroseconds(2);
  digitalWrite(8, HIGH);
  delayMicroseconds(5);
  digitalWrite(8, LOW);
  d1 = pulseIn(7, HIGH);
  cm1 = microsecondsToCentimeters(d1);
  Serial.print(cm1);
  Serial.print("cm1");
  Serial.println();
  digitalWrite(9, LOW);
  delayMicroseconds(2);
  digitalWrite(9, HIGH);
  delayMicroseconds(5);
  digitalWrite(9, LOW);
  d2 = pulseIn(10, HIGH);
  cm2 = microsecondsToCentimeters(d2);
  Serial.print(cm2);
  Serial.print("cm2"); //check cm2 is central ultrasonic
  Serial.println();
  digitalWrite(11, LOW);
  delayMicroseconds(2);
  digitalWrite(11, HIGH);
  delayMicroseconds(5);
  digitalWrite(11, LOW);
  d3 = pulseIn(12, HIGH);
  cm3 = microsecondsToCentimeters(d3);
  Serial.print(cm3);
  Serial.print("cm3");
  Serial.println();
  delay(100);
  int dif1;
  if (cm1 > cm3)
  {
    dif1 = cm1 - cm3;
  }
  else
  {
    dif1 = cm3 - cm1;
  }
  if (cm2 > 5 ||  cm2 < 2) // put a hump in front take the reading and replace 2
  {                         // put a hole in front take the reading and replace 5
    s = 1;
    Serial.print("Hump or hole detected");
  }
  else
  {
    s = 0;
  }
  if (dif1 < 10)
  {
    l = 0;
    r = 0;
  }
  else if (cm1 > cm3 && dif1 > 10)
  {
    r = 1;
  }
  else if (cm3 > cm1 && dif1 > 10)
  {
    l = 1;
  }
  if (Serial.available())
  {
    a = Serial.read();
    Serial.println(a);
  }

  if (a == 'f' && s==0)
  {

    digitalWrite(2, HIGH); // forword
    digitalWrite(3, LOW);
    digitalWrite(5, HIGH);
    digitalWrite(6, LOW);
  }
  else if (a == 'f' && s==1)
  {
    for(int i=0;i<5;i++)
    {
    digitalWrite(2, HIGH); //slow
    digitalWrite(3, LOW);
    digitalWrite(5, HIGH);
    digitalWrite(6, LOW);
    delay(25);
    digitalWrite(2, LOW); //slow
    digitalWrite(3, LOW);
    digitalWrite(5, LOW);
    digitalWrite(6, LOW);
    delay(75);
    } 
  }
  else if (a == 'r')
  {


    digitalWrite(3, HIGH); //right
    digitalWrite(2, LOW);

    digitalWrite(5, HIGH);
    digitalWrite(6, LOW);
  }
  else if (a == 'l' )
  {


    digitalWrite(2, HIGH); //left
    digitalWrite(3, LOW);

    digitalWrite(6, HIGH);
    digitalWrite(5, LOW);
  }
  else if (a == 'b')
  {


    digitalWrite(3, HIGH); //back
    digitalWrite(2, LOW);

    digitalWrite(6, HIGH);
    digitalWrite(5, LOW);
  }
  else
  {

    digitalWrite(3, LOW); //stop
    digitalWrite(2, LOW);

    digitalWrite(6, LOW);
    digitalWrite(5, LOW);
  }
}

long microsecondsToCentimeters(long microseconds) {
  return microseconds / 29 / 2;
}
