int s, l, r, a;
void setup()
{
  Serial.begin(9600);
  pinMode(8, OUTPUT);
  pinMode(7, INPUT);
  pinMode(9, OUTPUT);
  pinMode(10, INPUT);
  pinMode(11, OUTPUT);
  pinMode(12, INPUT);
  pinMode(21, OUTPUT);
  pinMode(22, OUTPUT);
  pinMode(23, OUTPUT);
  pinMode(24, OUTPUT);
  pinMode(25, OUTPUT);
  pinMode(26, OUTPUT);
}

void loop() {
  long d1, d2, d3, cm1, cm2, cm3;
  digitalWrite(8, LOW);
  delayMicroseconds(2);
  digitalWrite(8, HIGH);
  delayMicroseconds(5);
  digitalWrite(8, LOW);
  d1 = pulseIn(7, HIGH);
  cm1 = microsecondsToCentimeters(d1);
  Serial.print(cm1);
  Serial.print("cm1");
  Serial.println();
  digitalWrite(9, LOW);
  delayMicroseconds(2);
  digitalWrite(9, HIGH);
  delayMicroseconds(5);
  digitalWrite(9, LOW);
  d2 = pulseIn(10, HIGH);
  cm2 = microsecondsToCentimeters(d2);
  Serial.print(cm2);
  Serial.print("cm2");
  Serial.println();
  digitalWrite(11, LOW);
  delayMicroseconds(2);
  digitalWrite(11, HIGH);
  delayMicroseconds(5);
  digitalWrite(11, LOW);
  d3 = pulseIn(12, HIGH);
  cm3 = microsecondsToCentimeters(d3);
  Serial.print(cm3);
  Serial.print("cm3");
  Serial.println();
  delay(100);
  int dif1;
  if (cm1 > cm2)
  {
    dif1 = cm1 - cm2;
  }
  else
  {
    dif1 = cm2 - cm1;
  }
  if (cm1 < 5 ||  cm1 < 2)
  {
    s = 1;
  }
  else
  {
    s = 0;
  }
  if (dif1 < 10)
  {
    l = 0;
    r = 0;
  }
  else if (cm2 > cm3 && dif1 > 10)
  {
    r = 1;
  }
  else if (cm3 > cm2 && dif1 > 10)
  {
    l = 1;
  }
  a = Serial.read();
  if (a == 1 || s == 1 && l == 0 && r == 0)
  {
    digitalWrite(21, HIGH);
    digitalWrite(22, HIGH);
    digitalWrite(23, LOW);
    digitalWrite(24, HIGH);
    digitalWrite(25, HIGH);
    digitalWrite(26, LOW);
  }
  else if (a == 2 || l == 1)
  {

    digitalWrite(21, HIGH);
    digitalWrite(23, HIGH);
    digitalWrite(22, LOW);
    digitalWrite(24, HIGH);
    digitalWrite(25, HIGH);
    digitalWrite(26, LOW);
  }
  else if (a == 3 || r == 1)
  {

    digitalWrite(21, HIGH);
    digitalWrite(22, HIGH);
    digitalWrite(23, LOW);
    digitalWrite(24, HIGH);
    digitalWrite(26, HIGH);
    digitalWrite(25, LOW);
  }
  else if (a == 4)
  {

    digitalWrite(21, HIGH);
    digitalWrite(23, HIGH);
    digitalWrite(22, LOW);
    digitalWrite(24, HIGH);
    digitalWrite(26, HIGH);
    digitalWrite(25, LOW);
  }
  else
  {
    digitalWrite(21, LOW);
    digitalWrite(23, HIGH);
    digitalWrite(22, LOW);
    digitalWrite(24, LOW);
    digitalWrite(26, HIGH);
    digitalWrite(25, LOW);
  }
}

long microsecondsToCentimeters(long microseconds) {
  return microseconds / 29 / 2;
}
