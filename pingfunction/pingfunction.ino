int s, l, r, a;
void setup()
{
  Serial.begin(9600);
  pinMode(8, OUTPUT);
  pinMode(7, INPUT);  //8 and 7 for right sensor
  pinMode(9, OUTPUT);
  pinMode(10, INPUT); //10 and 9 for  central sensor
  pinMode(11, OUTPUT);
  pinMode(12, INPUT);//12 AND 11 for left sensor
  }

void loop() {
  long d1, d2, d3, cm1, cm2, cm3;  //cm1 is right distance cm2 is central and cm3 is left
  d1=ultrasonic(8,7);
  cm1 = microsecondsToCentimeters(d1);
  d2=ultrasonic(9,10);
  cm2 = microsecondsToCentimeters(d2);
  d3=ultrasonic(11,12);
  cm3 = microsecondsToCentimeters(d3);
  Serial.print(cm1);
  Serial.println("cm1");
  Serial.print(cm2);
  Serial.println("cm2");
  Serial.print(cm3);
  Serial.println("cm3");
}  
long ultrasonic(int a,int b)
{
  int d1;
  digitalWrite(a, LOW);
  delayMicroseconds(2);
  digitalWrite(a, HIGH);
  delayMicroseconds(5);
  digitalWrite(a, LOW);
  d1 = pulseIn(b, HIGH);
  return d1;
}
long microsecondsToCentimeters(long microseconds) {
  return microseconds / 29 / 2;
}
